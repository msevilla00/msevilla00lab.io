---
layout: post
title:  "Estadísticas de la página"
date:   2021-09-17
categories: web
---

Casi ha pasado un año y mi proyecto de página personal y blog parece que no arranca. Afortunadamente nadie más que yo estoy atento de la misma y aún no la he publicitado más allá de compartir algún enlace del [apartado de presentaciones](./teaching/).

En su día quise dejarla lista para cargar materiales que me urgían que estuvieran públicos, luego estuve entretenido con otras cosas y ya va siendo hora de hacer alguna cosa más aquí. 

Parte de la tardanza en "activar" la web estaba directamente relacionada con la ausencia de un sistema de estadísticas que me permitiera analizar si alguien más que yo llegaba aqui y no quería usar la telemetría de Google Analytics por cuestiones de respeto a la privacidad de potenciales visitantes.

La cuestión es que entre las alternativas a Google casi todas caían en el uso sistemas más o menos intrusivos y de pago.

La cuestión es que encontré que en la asociación [Montera34](https://montera34.org), de la que soy parte desde hace un tiempo usando su servicio de [nextcloud](https://es.wikipedia.org/wiki/Nextcloud), tienen montado en su servidor una instancia de [matomo](https://matomo.org/).

Matomo, como [podemos leer en Wikipedia](https://es.wikipedia.org/wiki/Matomo) se trata de una aplicación libre y de código abierto que sirve para analizar las visitas en sitios webs con una interfaz sencilla y con multitud herramientas. Dejo aquí una captura del aspecto de la herramienta tal y como se muestra en wikipedia:

![Matomo Dashboard Screeenshot](https://upload.wikimedia.org/wikipedia/commons/7/75/Piwik_screenshot_german.png)

La verdad es que llevo menos de mes con el código de javascript insertado en estas páginas y a penas hay entradas a mi web, pero es mucho más significativo e interesante los informes que hemos podido encontrar en la otra página web a la que le hemos incluido este servicio de Montera34: la [página web de la asociación QGIS España](https://qgis.es) que aprovecho para invitar a quien lea esto a visitar.

Y por ahora es todo... espero poder incluir en otra entrada reciente algunos enlaces de interés que quiero tener a mano para mi propia conveniencia.

Hasta otra.

M