---
layout: post
title:  "Empezamos"
date:   2020-12-30
categories: web
---

Cuando solo queda poco más de un día de este fatídico año cuya cifra, por otro lado me ha fascinado (veinte-veinte) me he decidido, tras varios días trasteando con ello, a poner en marcha esta página web persona, en principio, como esto como blog.

Como comento en el apartado de `about` que pueden encontrar más arriba, en principio esto lo estoy montando en [jekyll][jekyll] en GitLab (hace tiempo que me decidí a usar esta plataforma sobre la más popular GitHub). Hace tiempo que tenía pendiente iniciar esto de algún modo y me pareció que Jekyll era lo suficientemente sencillo, en especial en lo que respecta a los archivos en MarkDown que se incluyen como post, como para lanzarme  usarlo directamente, y por el momento con una plantilla muy básica.

En este comienzo he de mencionar a tres personas, repositorios o materiales, que he tenido presente al inicio de esta andadura: 
- [Carlos Cámara](https://www.carloscamara.es), que si llega a ver esto me dirá que me pase a Hugo y del que tengo muchas cosas que aprender de lo que lleva haciendo con estas y otras herramientas;
- [Joan Cano](https://joancano.github.io/), que hace tiempo me adelantó en ir poniendo parte de los apuntes en su página de GitHub;
- y [Jorge Sanz](https://jorgesanz.net), geoinquieto por antonomasia, del que he podido aprender un sinfin de cosas de la gran cantidad de materiales que tiene públicos en su usuario de [GitHub](https://github.com/jsanz), entre otros, y por fin estos días pude ver con detalle el vídeo que hizo para Geoinquietos Valencia sobre _GitHub Pages_ y cuya entrada está en el blog de [geomaticblog.net](https://geomaticblog.net/2020/04/07/charla-introduccion-a-github-pages/).

Y eso es todo para dejar una primera entrada en el blog.

Ah! y **¡Feliz 2021!**

[jekyll]: http://jekyllrb.com/
