# Repositorio en pruebas

Página personal y blog en GitLab de Miguel Sevilla-Callejo


# Notas

Modificación sobre la plantilla de [Jekyll de GitLab](https://gitlab.com/pages/jekyll)

```bash
# creación de la carpeta assets, _drafts
mkdir assets _drafts

# añadir Gemfile.lock a .gitignore
echo "Gemfile.lock" >> .gitignore

# borrar Gemfile.lock
rm Gemfile.lock

# servir página (se genera un nuevo archivo Gemfile.lock)
bundle exec jekyll serve
```


<!-- POR EDITAR

Cambiar configuración de la página

 -->

## Correr Jekyll en local 

### Instalando Ruby y dependencias

`bundle exec jekyll serve`

Si se modifica el archivo _config.yml hay que volver a correr el proceso

### Con imagen Docker

Se ha añadido archivo `docker-compose.yml`

También se puede hacer tirando de `docker run` del siguiente modo:

Construir la página web:

```bash
docker run --rm \
    --volume="$PWD:/srv/jekyll" -it \
    jekyll/jekyll:3.5 jekyll serve --watch --drafts
```

<!-- para poder alojar sin problemas de la dirección del servidor revisar _config.yml -->


<!--  NO FUNCIONA / Revisar

Servir la página en local:

```bash
docker run --rm \
    --volume="$PWD:/srv/jekyll" -it \
    jekyll/jekyll:3.5 jekyll build
```
-->

## Enlaces de interés

- [GitHub - jsanz/gh-pages-minima-starter: A minimal example for running Github Pages with the minima theme.](https://github.com/jsanz/gh-pages-minima-starter)

- [Running Jekyll in Docker – Davy's Tech Blog](https://ddewaele.github.io/running-jekyll-in-docker/)