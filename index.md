---
layout: default
permalink: /
---

<img src="assets/miguel_tv.jpg" style="height:200px;width:200px;float:left;margin:10px" alt="Fotografía Miguel Sevilla-Callejo">

<!-- ![Fotografía Miguel Sevilla-Callejo](assets/miguel_tv.jpg)
 -->
Soy **Miguel Sevilla-Callejo**, geógrafo, investigador, docente y consultor... pero sobre todo, persona entusiasta de las tecnologías y los datos libres y abiertos.

Me gusta aprender cosas nuevas, enseñar, compartir y colaborar en proyectos como [OpenStreetMap](https://wiki.openstreetmap.org/wiki/ES:Espa%C3%B1a), la comunidad de usuarios de [QGIS España](https://qgis.es) o dinamizar actividades en el grupo [Mapeado Colaborativo / Geoinquietos Zaragoza](https://mapcolabora.org), entre otros.

Esta pretende ser mi página y blog en internet que como verás está aún en construcción.

Los contenidos que puedes encontrar aquí son para poder centralizar algunas de los materiales y pensamientos que tengo dispersos por ahí y, en buena medida, para mi mismo, para mi yo del futuro que pueda consultar rápidamente algunas de las notas y averiguaciones que voy haciendo.

## Mi tesis doctoral

<!-- ![Portada tesis](assets/tesis_cotapata/portada_tesis_mini.jpg) -->

<img src="assets/tesis_cotapata/portada_tesis_mini.jpg" style="width:100px;float:right;margin:10px" alt="Portada Tesis">

Quizá hayas llegado aquí buscando los archivos de mi tesis doctoral que hace tiempo tuve subidos en mi repositorio de GitHub. Por si acaso aquí pongo los enlaces a la nueva ubicación.

Título: _Organización territorial y campesinado en el Parque Nacional y Área Natural de Manejo Integrado Cotapata (Bolivia)_

<!-- se renderiza como tabla -->

Memoria escrita:     [PDF - 25 MB](assets/tesis_cotapata/MSC2010_Organizacion_territorial_y_campesinado_en_PNANMI_Cotapata_Bolivia.pdf) | Anexo cartográfico:     [PDF - 33 MB](assets/tesis_cotapata/MSC2010_Atlas_PNANMI_Cotapata.pdf) | Presentación de defensa:     [PDF - 13MB](assets/tesis_cotapata/MSC2010_Presentacion_Tesis_Cotapata.pdf)

Enlace permanente en [repositorio de la Universidad Autónoma de Madrid](http://hdl.handle.net/10486/5726)


## Blog

<div class="home">
<!-- 
  <h1 class="page-heading">Entradas del blog:</h1>
 -->
  <ul class="post-list">
    {% for post in site.posts %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

        <h3>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h3>
      </li>
    {% endfor %}
  </ul>

  <p class="rss-subscribe">Subscribirse <a href="{{ "/feed.xml" | prepend: site.baseurl }}">vía RSS</a></p>

</div> 
