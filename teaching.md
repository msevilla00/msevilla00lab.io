---
layout: page
title: Cursos y charlas
permalink: /teaching/
---

Esta página tiene como objetivo recoger los materiales y presentaciones de mis cursos y charlas. **Aún está en construcción** y espero tenerla actualizada en breve.

La parte más importante son los materiales de mi docencia en la **Universidad de Zaragoza** entre los años 2016 y 2020 pero como se podrá ver, también tengo idea de añadir otros materiales como las charlas sobre cartografía colaborativa en OpenStreetMap u otros talleres que he impartido en los últimos años.


**Contenidos**

<!-- TOC -->
- [Máster en Tecnologías de la Información Geográfica](#máster-en-tecnologías-de-la-información-geográfica)
  - [Módulo de introducción a los SIG libres](#módulo-de-introducción-a-los-sig-libres)
    - [Presentaciones](#presentaciones)
    - [Ejercicios y otros archivos](#ejercicios-y-otros-archivos)
  - [Módulo de scripting para el análisis espacial: introducción a Python](#módulo-de-scripting-para-el-análisis-espacial-introducción-a-python)
  - [Módulo Usando R para el análisis espacial](#módulo-usando-r-para-el-análisis-espacial)
    - [Presentaciones](#presentaciones-1)
    - [Materiales](#materiales)
  - [Módulo Herramientas para la visualización online de la información geográfica](#módulo-herramientas-para-la-visualización-online-de-la-información-geográfica)
- [Presentaciones Jornadas IPErinas](#presentaciones-jornadas-iperinas)
  - [OpenStreetMap](#openstreetmap)
  - [Uso de programas libres en ciencia](#uso-de-programas-libres-en-ciencia)
- [Presentación sobre la aplicación CitMApp en Jornadas de SIG Libre 2021](#presentación-sobre-la-aplicación-citmapp-en-jornadas-de-sig-libre-2021)
- [Más materiales](#más-materiales)
<!-- /TOC -->


<!-- Máster UNIZAR 2020 -->

## Máster en Tecnologías de la Información Geográfica

A continuación se adjuntan enlaces públicos a diversas presentaciones en formato web (mayoritariamente sobre reveal.js) y en PDF realizadas por mi, Miguel Sevilla-Callejo, y que se encuentran, si no se especifica lo contrario, bajo licencia [Creative Commons Atribución Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).

<!-- <img src="../assets/teaching/presentacion_mtig_python.png" style="width:350px" alt="Diapositiva presentación Python"> -->

Las presentaciones y el material pertenecen al curso 2019-2020 del [Máster Universitario en Tecnologías de la Información Geográfica para la Ordenación del Territorio: Sistemas de Información Geográfica y Teledetección](https://estudios.unizar.es/estudio/ver?id=608) del Departamento de [Geografía y Ordenación del territorio de la Universidad de Zaragoza](https://geografia.unizar.es).

<!-- módulo QGIS -->

### Módulo de introducción a los SIG libres

Asignatura 60402. _Análisis de la información geográfica: SIG_

Módulo 3.5 / Curso 2019-20

Módulo de introducción a los sistemas de información geográfica sobre programas libres y de código abierto, específicamente, con QGIS.


<img src="https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/slide_intro_sig_libres.png" style="width:250px">


#### Presentaciones

Enlaces a las presentaciones en formato PDF

0. [Introducción a los SIG libres](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/0_introduccion_a_los_sig_libres.pdf)
1. [Presentación a QGIS 3.x](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/01_qgis_presentacion.pdf)
2. [Procesamiento simple y creación cartográfica](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/02_qgis_procesamiento_simple_y_cartografía.pdf)
3. [Procesamiento avanzado, plug-ins y más](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/03_qgis_procesamiento_avanzado.pdf)

#### Ejercicios y otros archivos

- [Ejercicios a entregar](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicios_mod35_intro_qgis_2020.pdf)
- [Archivos del ejercicio 1](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicio1.7z)
- [Archivos del ejercicio 2](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicio2.7z)
- [Ejemplo de proyecto de QGIS con un atlas de las Comarcas de Aragón](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/atlas_comarcas.zip)
- Datos de ejemplo: [Epidemia de cólera de Londres de 1854](https://en.wikipedia.org/wiki/1854_Broad_Street_cholera_outbreak): [snowgis_gpkg.7z](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/snowgis_gpkg.7z)

<!-- módulo Python -->

### Módulo de scripting para el análisis espacial: introducción a Python

Asignatura 60402. _Análisis de la información geográfica: SIG_

Módulo 3.6a / Curso 2019-20

Módulo de introducción a la consola del sistema, los aspectos más básicos de la programación y específicamente, primeros pasos con Python y su aplicación al análisis de datos espaciales. 

<img src="https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/slide_intro_pyqgis.png" style="width:250px">

<!-- #### Presentaciones -->

0. [_Scripting_](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/00_scripting.html)
1. [Presentación de Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/01_intro_python.html)
2. [Datos y Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/02_datos_python.html)
3. [Objetos en Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/03_objetos_python.html)
4. [Intro a PyQGIS](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/04_pyqgis_intro.html)
5. [Más sobre Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/05_mas_python.html)

<!-- INCLUIR MATERIALES -->



<!-- módulo R -->

### Módulo Usando R para el análisis espacial

Asignatura 60402. Análisis de la información geográfica: SIG

Módulo 3.6b / Curso 2019-20

Módulo de introducción al lenguaje R, al manejo de RStudio y a la composición de mapas sencillos con esta herramienta.

<img src="https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/slide_intro_r.png" style="width:250px">


#### Presentaciones

1. [Presentación de R](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/01_r_intro.html)
2. [_Scripting_ y _data frames_](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/02_r_scripting_dataframes.html)
3. [_dplyr_ y gráficos](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/03_r_dplyr_graficos.html)
4. [Introducción a R espacial](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/04_r_intro_espacial.html)

#### Materiales

- Archivo con **información vectorial** de ejemplo en formato [GeoPackage](https://en.wikipedia.org/wiki/GeoPackage): [vectors.gpkg](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/vectors.gpkg)
- Archivo con **información raster** de ejemplo en formato [GeoTIFF](https://en.wikipedia.org/wiki/GeoTIFF): [raster.tif](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/raster.tif)
- **Tabla ejemplo** de datos internacionales en formato CSV: [gapminder_data.csv](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/gapminder_data.csv)
- Planteamiento del **ejercicio de evaluación** del módulo de R del máster: [ejercicios_r_master_tig_2020.pdf](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/ejercicios_r_master_tig_2020.pdf)
- Indicadores de desarrollo humano de 2018 en formato Excel (copia de descarga de web UNDP): [2018_all_indicators.xlsx](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/2018_all_indicators.xlsx)


<!-- módulo visualización online -->

### Módulo Herramientas para la visualización online de la información geográfica

Asignatura 60418. Visualización, presentación y difusión de la información geográfica.

Módulo 5.3 / Curso 2019-20

Módulo de introducción a la representación de información online; primeros pasos con HTML, iniciativas de datos abiertos en la red, sistemas cliente-servidor de datos espaciales y composición básica de mapas online con leaflet.

<img src="https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/slide_intro_leaflet.png" style="width:250px">

<!-- #### Presentaciones -->

1. [Introducción HTML](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/01_intro_web.html)
2. Introducción a OSM (pendiente de incluir enlace a material)
3. [Introducción a PostGIS](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/03_intro_postgis.html)
4. [Montando mapas con Leaflet](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/04_intro_leaflet.html)

<!-- INCLUIR MATERIALES -->


## Presentaciones Jornadas IPErinas

### OpenStreetMap

![](https://digital.csic.es/retrieve/1359391/SevillaCallejo_OpenStreetMap_JIPErinas2016.pdf.jpg)

Presentación elaborada para las **Jornadas IPErinas 2016**, celebradas el 15 de diciembre de 2016 en Zaragoza y disponible en el repositorio de [DIGITAL.CSIC](https://digital.csic.es/handle/10261/143024)

### Uso de programas libres en ciencia

![](https://digital.csic.es/retrieve/1130599/programas_libres_msevillacallejo.pdf.jpg)

Presentación elaborada para las **III Jornadas IPErinas**, celebradas el 11 de diciembre de 2014. y disponible en el repositorio de [DIGITAL.CSIC](https://digital.csic.es/handle/10261/108910)


## Presentación sobre la aplicación CitMApp en Jornadas de SIG Libre 2021

![](http://diobma.udg.edu/bitstream/handle/10256.1/6221/6221.mp4.jpg?sequence=5)


**Título:** *CitMApp. Una aplicación libre al servicio de proyectos de ciencia ciudadana de carácter espacial*

**Autores:** Miguel Sevilla-Callejo, Jorge Barba, Francisco Sanz, Victor Val, Jonatan Val, Daniel Bruno, Enrique Navarro (Instituto Pirenaico de Ecología - CSIC, Mapeado Colaborativo - Geoinquietos Zaragoza, Fundación Ibercivis)

**Resumen:** 

Uno de los problemas crecientes entre los proyectos de ciencia ciudadana está siendo la resolución de la toma de datos sobre el terreno dado que muchos de ellos tienen un componente espacial significativo. Estos proyectos requieren de la toma de fotografías e información de diversa índole sobre un punto geográfico.

Algunos de los proyectos de ciencia ciudadana desarrollan aplicaciones específicas para que los voluntarios las instalen en sus teléfonos móviles. Esta solución es muy práctica pues aprovecha la capacidad de posicionamiento de estos dispositivos así como la ejecución de programas para la recogida de datos. Sin embargo, el desarrollo específico de aplicaciones para móviles suele estar restringido al presupuesto de los proyectos y suelen estar diseñadas específicamente para cada una de ellos. Esto nos lleva a que si se participa en varios proyectos se ha de tener instaladas varias aplicaciones, cada una con sus especificaciones propias.

Desde Mapeado Colaborativo - Geoinquietos Zaragoza (mapcolabora.org) junto con la Fundación Ibercivis (ibercivis.es) planteamos la creación de una plataforma común que permitiera la recogida de datos georeferenciados para diversos proyectos de ciencia ciudadana. CitMApp es la aplicación resultante de esta idea.

CitMApp es una aplicación para dispositivos Android, libre y de código abierto (github.com/Ibercivis/CitMApp) que permite la recogida de datos en tiempo real y favorece la puesta en marcha de análisis científicos para proyectos de ciencia ciudadana. Los usuarios recogen la posición y actividad, realizan mediciones o captan imágenes relevantes de fenómenos de su interés que podrán ser ampliados por la comunidad de usuarios.

La aplicación utiliza como base la librería OSMdroid y hace uso del OSMBonusPack. Además, para la visualización de imágenes on-line utiliza la librería Picasso y para los HTTP Request emplea la librería Volley. Los datos se gestionan en los servidores de Ibercivis.

En CitMApp cada proyecto tiene su espacio en un formulario específico. Sobre la misma app, sin necesidad de desarrollos propios, sin complicaciones y de forma sencilla se pueden montar tareas propias, y una vez introducidos los datos sobre el terreno, solicitar una tabla con toda la información disponible.

El proyecto para la monitorización de la calidad de las aguas en Aragón, #RiosCiudadanos (riosciudadanos.csic.es), es la primera iniciativa que ha utilizado esta aplicación para la consecución de sus objetivos. En este proyecto del Instituto Pirenaico de Ecología - CSIC, cerca de 80 voluntarios están tomando muestras de agua en más de un centenar de puntos de la geografía aragonesa, haciendo uso de CitMApp para subir los datos obtenidos y procesados.

En la comunicación, presentamos la aplicación, su funcionamiento y potencialidades, así como los aspectos técnicos más relevantes. Además, mostramos los primeros resultados obtenidos con CitMApp en el proyecto #RiosCiudadanos.

[Enlace al vídeo de la presentación](http://diobma.udg.edu/handle/10256.1/6221)


## Más materiales

En construcción