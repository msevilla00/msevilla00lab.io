---
layout: page
permalink: /publications/
title: Publicaciones
---

<!-- TOC -->

- [Artículos en revistas científicas](#art%C3%ADculos-en-revistas-cient%C3%ADficas)
- [Libros](#libros)
- [Capítulos de libros](#cap%C3%ADtulos-de-libros)
- [Enlaces a mis perfiles de publicaciones](#enlaces-a-mis-perfiles-de-publicaciones)

<!-- /TOC -->

## Artículos en revistas científicas

<!-- Listado ANECA y comentado el índice de impacto -->

<!-- 20 -->
**González-Sampériz, P.; Gil-Romera, G.;García-Prieto, E.; Aranbarri, J.; Moreno, A.; Morellón, M.; <u>Sevilla-Callejo, M.</u>; Leunda, M.; Santos, L.; Franco-Múgica, F.; Andrade, A.; Carrión, J.; Valero-Garces, B.L. (2020).** "Strong continentality and effective moisture drove unforeseen vegetation dynamics since the last interglacial at inland Mediterranean areas: the Villarquemado sequence in NE Iberia", *Quaternary Science Reviews*, 242: 106425. <https://doi.org/10.1016/j.quascirev.2020.106425>
<!--
Scopus 2019 impacto: 7,4
JCR: año 2019, posición en el área GEOGRAPHY, PHYSICAL oct-50 (Q1), índice de impacto: 4,878, 0 citas (202008)
-->

<!-- 19 -->
**Cakir, R.; Raimonet, M.; Sauvage, S.; Paredes-Arquiola, J.; Grusson, Y.; Roset, L.; Meaurio, M.; Navarro, E.; <u>Sevilla-Callejo, M.</u>; Lechuga-Crespo, J.L.; Gomiz Pascual, J.J.; Bodoque, J.M.; Sánchez-Pérez, J.M. (2020)** "Hydrological Alteration Index as an Indicator of the Calibration Complexity of Water Quantity and Quality Modeling in the Context of Global Change", *Water*, 12 (1): 115. <https://doi.org/10.3390/w12010115>
<!--
 JCR: año 2019, posición en el área WATER RESOURCES 31/94 (Q2), índice de impacto: 2,544, 2 citas (202008)
-->

<!-- 18 -->
**Gil-Romera, G. Adolf, C.; Benito, B.M.; Bittner, L.; Johansson, M.U.; Grady, D.A.; Lamb, H.F.; Lemma, B.; Fekadu, M.; Glaser, B.; Mekonnen, B.; <u>Sevilla-Callejo, M.</u>; Zech, M.; Zech, W.; Miehe, G. (2019)** "Long-term fire resilience of the Ericaceous Belt, Bale Mountains, Ethiopia" _Biology Letters_, 15 (7): 20190357. <https://doi.org/10.1098/rsbl.2019.0357>
<!-- 
JCR: año 2019, posición en el área ECOLOGY 49/168, Q2, índice de impacto: 2,869, 3 citas (202008)
-->

<!-- 17 -->
**Oliva-Urcia, B.; Moreno, A.; Leunda, M.; Valero-Garcés, B.; González-Sampériz, P.; Gil-Romera, G.; Mata M.P.; Adsuar, A.; Aranbarri, Barreiro, F.; Bartolomé, M.; Bueno, B.; García-Prieto, E.; García, B.; Larrasoaña, J.C.; Parés, J.M.; Pérez, A.; Rico, M.; Salabarnada, A.; Salazar, A.; <u>Sevilla-Callejo, M.</u>; Tarrats, P. (2018).** "Last deglaciation and Holocene environmental change at high altitude in the Pyrenees: the geochemical and paleomagnetic record from Marboré Lake (N Spain)" _Journal of Paleolimnology_ 59: 349–371. <https://doi.org/10.1007/s10933-017-0013-9>
<!-- 
Scopus: sin percentil, índice de impacto: 1,31, 4 citas
JCR: año 2009, posición en el área GEOSCIENCES, MULTIDISCIPLINARY 98/196, índice de impacto: 2,009, 7 citas (202002)
-->


<!-- 16 -->
**Leunda, M.; González-Sampériz, P.; Gil Romera, G.; Aranbarri, J.; Morena, A.; Oliva-Urcia,B. ; <u>Sevilla-Callejo, M.</u>; Valero-Garcés, B. (2017).** “The Late-Glacial and Holocene Marboré Lake sequence (2612 m a.s.l., CentralPyrenees, Spain)” _Global and Planetary Change_, 157: 214-231. <https://doi.org/10.1016/j.gloplacha.2017.08.008>
<!--
Scopus: percentil 65 (Earth & Planetary Sciences), índice de impacto: 1,08, 7 citas
JCR: año 2018, posición en el área GEOSCIENCES, MULTIDISCIPLINARY 23/196, índice de impacto: 4,1, 12 citas (202008)
-->

<!-- 15 -->
**González-Sampériz, P.; Aranbarri, J.; Pérez-Sanz, A.; Gil-Romera, G.; Moreno, A.; Leunda, M.; <u>Sevilla-Callejo, M.</u>; Corella, J.M.; Morellón, M.; Oliva, B.; Valero-Garcés, B. (2017).** “Environmental and climate change in the southern Central Pyrenees since the Last Glacial Maximum: A view from the lake records” _Catena_, 149: 668-688. <https://doi.org/10.1016/j.catena.2016.07.041>
<!-- 
Scopus: percentil 99 (Earth & Planetary Sciences), índice de impacto: 8,71, 50 citas
JCR: año 2018, posición en el área GEOSCIENCES, MULTIDISCIPLINARY 23/196, índice de impacto: 3,851, 54 citas (202008)
-->

<!-- 14 -->
**Aranbarri, J.; González-Sampériz, P.; Valero-Garcés, B.; Moreno, A.; Gil-Romera, G.; <u>Sevilla-Callejo, M.</u>;García-Prieto, E.; Di Rita, F.; Mata, M.P.; Morellón, M.; Magri, D., Rodríguez-Lázaro, J.; Carrión, J.S. (2015).** “Human–landscape interactions in the Conquezuela–Ambrona Valley (Soria, continental Iberia): From the early Neolithic land use to the origin of the current oak woodland”. _Palaeogeography, Palaeoclimatology, Palaeoecology_ 436:41–57. <https://doi.org/10.1016/j.palaeo.2015.06.030>
<!--
Scopus: percentil 65 (Environmental Science), índice de impacto: 1.34, 13 citas 
JCR: año 2014, posición en el área GEOGRAPHY, PHYSICAL 20/46, índice de impacto: 2,339, 15 citas (202008)
-->

<!-- 13 -->
**Gil-Romera, G.; González-Sampériz, P.; Lasheras-Álvarez, L.; <u>Sevilla-Callejo, M.</u>; Moreno, A.; Valero-Garcés, B.; López-Merino, L.; Carrión, J.S.; Pérez Sanz, A.; Aranbarri, J.; García-Prieto Fronce; E. (2014).** “Biomass-modulated fire dynamics during the Last Glacial–Interglacial Transition at the Central Pyrenees (Spain)”. _Palaeogeography, Palaeoclimatology, Palaeoecology_ 402:113–124. <https://doi.org/10.1016/j.palaeo.2014.03.015>
<!-- 
Scopus: percentil 99 (Environmental Sciences), índice de impacto: 2.22, 38 citas
JCR: año 2014, posición en el área GEOGRAPHY, PHYSICAL 20/46, índice de impacto: 2,339, 41 citas (202002)
-->

<!-- 12 -->
**Gil-Romera, G.; Neumann, F.H.; Scott,L.; <u>Sevilla-Callejo, M.</u>; et al. (2014).** “Pollen taphonomy from hyaena scats and coprolites: preservation and quantitative differences”. _Journal of Archaeological Science_ 46:89–95. <https://doi.org/10.1016/j.jas.2014.02.027>
<!--
Scopus: percentil 65 (Social sciences), 82 (History), índice de impacto: 1,26, 6 citas
JCR: año 2014, posición en el área ANTHROPOLOGY 11/84, índice de impacto: 2,196, 11 citas (202008)
-->

<!-- 11 -->
**Aranbarri, J.; González-Sampériz, P.; Valero-Garcés, B.; Moreno, A.; Gil-Romera, G.; <u>Sevilla-Callejo, M.</u>; García-Prieto, E.; Di Rita, F.; Mata, M.P.; Morellón, M.; Magri, D., Rodríguez-Lázaro, J.; Carrión, J.S.(2014).** “Rapid climatic changes and resilient vegetation during the Lateglacial and Holocene in a continental region of south-western Europe”. _Global and Planetary Change_ 114:50–65. <https://doi.org/10.1016/j.gloplacha.2014.01.003>
<!--
Scopus: percentil 97 (Earth & Planetary Sciences), índice de impacto: 3,67, 65 citas
JCR: año 2014, posición en el área GEOSCIENCES, MULTIDISCIPLINARY 34/175, índice de impacto: 2,766, 71 citas (202008)
-->

<!-- 10 -->
**Pérez-Sanz, A.; González-Sampériz, P.; Moreno, A.; Valero-Garcés, B.; Gil-Romera, G.; Rieradevall, M.; Tarrats, P.; Lasheras-Álvarez, L.; Morellón, M.; Belmonte, A.; Sancho, C.; <u>Sevilla-Callejo, M.</u>; Navas, A. (2013).** “Holocene climate variability, vegetation dynamics and fire regime in the central Pyrenees: the Basa de la Mora sequence (NE Spain)”. _Quaternary Science Reviews_ 73: 149-169. <https://doi.org/10.1016/j.quascirev.2013.05.010>
<!--
Scopus: percentil 99 (Social Sciences), índice de impacto 4,27, 65 citas
JCR: año 2013, posición en el área GEOGRAPHY, PHYSICAL 3/46, índice de impacto: 4,571, 77 citas (202008)
-->

<!-- 9 -->
**Lasheras-Álvarez, L.; Pérez-Sanz, A.; Gil-Romera, G.; González-Sampériz, P.; <u>Sevilla-Callejo, M.</u>; Valero-Garcés, B. (2013).** “Historia del fuego y la vegetación en una secuencia holocena del Pirineo central: la Basa de la Mora”. _Cuadernos de Investigación Geográfica_ 39 (1):77–95. <http://bit.ly/CIG-39-1-p77>
<!--
Scopus: percentil 80 (Social sciences), índice de impacto: 0,91, 8 citas
SJR: año 2013 posición en el área EARTH AND PLANETARY SCIENCES 123/258, índice de impacto: 0,28, 8 citas
-->

<!-- 8 -->
**González-Sampériz, P.; García-Prieto, E.; Aranbarri, J.; Valero-Garcés, B.L.; Moreno, A.; Gil-Romera, G.; <u>Sevilla-Callejo, M.</u>; Santos, L.; Morellón, M.; Mata, M.P.; Andrade, A.; Carrión, J.S. (2013).** Reconstrucción paleoambiental del último ciclo glacial-interglacial en la Iberia continental: la secuencia del Cañizar de Villarquemado (Teruel). Cuadernos de Investigación Geográfica 39 (1):49–76. <http://bit.ly/CIG-39-1-p49>
<!--
Scopus: percentil 65 (Social Sciences), índice de impacto 0,75, 7 citas
SJR: año 2013 posición en el área EARTH AND PLANETARY SCIENCES 123/258, índice de impacto: 0,28, 7 citas (202002)
-->

<!-- 7 -->
**<u>Sevilla-Callejo, M.</u> (2012).** “Delimitación de la vegetación, los usos y las coberturas del terreno en un área de montaña tropical. El caso de la aplicación de las TIG al PN y ANMI Cotapata (Bolivia)”. _Revista Geográfica de Valparaíso_ 45: 65–82. <http://www.rgv.ucv.cl/rgv45.html>
<!--
Impacto (otros indícios): La revista tiene evaluación externa por pares, tiene un comité científico internacional, el contenido de la revista es exclusivo de investigación y se encuentra en repertorios y boletines bibliográficos vinculados a la Geografía.
-->

<!-- 6 -->
**Gil-Romera, G.; Turton, D.; <u>Sevilla-Callejo, M.</u> (2011).** “Landscape change in the lower Omo valley, southwestern Ethiopia: burning patterns and woody encroachment in the savanna”. _Journal of Eastern African Studies_, 5(1): 108–128. <https://doi.org/10.1080/17531055.2011.544550>
<!--
Scopus: percentil 55 (Ciencias sociales), 75 (Cultural Studies), índice de impacto: 1,29, 10 citas
JCR: año 2011, posición en el área AREA STUDIES 40/66, índice de impacto: 0,333, 6 citas (202008)
-->

<!-- 5 -->
**Pérez-Sanz, A.; González-Sampériz, P.; Valero-Garcés, B.L.; Moreno, A.; Morellón, M.; Sancho, C.; Belmonte, A.; Gil-Romera, G.;<u>Sevilla-Callejo, M.</u>; Navas, A. (2011).** “Clima y actividades humanas en la dinámica de la vegetación en los últimos 2000 años en el Pirineo Central: el registro palinológico de la Basa de la Mora (Macizo de Cotiella)”. _Zubía_ 23: 17–38. <http://bit.ly/zubia23>
<!--
Impacto (otros indícios): La revista tiene evaluación externa por pares, tiene un comité científico internacional, el contenido de la revista es exclusivo de investigación y se encuentra en repertorios y boletines bibliográficos vinculados a la geografia, en concreto esta dentro de en concreto esta dentro de DIALNET y LatIndex ademas de que se encuentra dentro de la clasificación integrada de revistas científicas (CIRC) dentro del grupo B.
-->

<!-- 4 -->
**Gil-Romera, G.; Carrión, J.S.; Pausas, J.G.; <u>Sevilla-Callejo, M.</u>; Lamb, H.F.; Fernández, S.; Burjachs, F. (2010).** Holocene fire activity and vegetation response in South-Eastern Iberia. Quaternary Science Reviews, 29(9-10) ,1082–1092. <https://doi.org/10.1016/j.quascirev.2010.01.006>
<!--
Scopus: percentil 92 (Environmental Science), 94 (Agricultural...), índice de impacto: 4.27, 6 citas
JCR: año 2010, posición en el área GEOGRAPHY, PHYSICAL 2/42, índice de impacto: 4,657, 68 citas (202008)
-->

<!-- 3 -->

**Gil-Romera, G.; Lamb, H.F.; Turton, D.; <u>Sevilla-Callejo, M.</u>; Umer, M. (2010).** “Long-term resilience, bush encroachment patterns and local knowledge in a Northeast African savanna”. _Global Environmental Change_ 20(4): 612–626. <https://doi.org/10.1016/j.gloenvcha.2010.04.008>
<!-- 
Scopus: percentil 75 (Geography, Planning and Development), índice de impacto: 1,36, 18 citas
JCR: año 2011, posición en el área Environmental Sciences 6/193, índice de impacto: 4,918, 19 citas (202008)
-->

<!-- 2 -->
**Mata Olmo, R.; <u>Sevilla Callejo, M.</u> (2009).** “Integración regional, conflictos locales y planificación. El caso del Parque Nacional Cotapata y el proyecto viario La Paz-Guayaramerín (Bolivia)”. _Anuario Americanista Europeo_ 6-7: 509-527. <http://bit.ly/AAE-6-7-p509>
<!-- impacto: 
La revista tiene evaluación externa por pares, tiene un comité científico internacional, el contenido de la revista es exclusivo de investigación y se encuentra en repertorios y boletines bibliográficos vinculados a la geografia, en concreto esta dentro de LatIndex ademas de que se encuentra dentro de la Clasificación Integrada de Revistas Científicas (CIRC) dentro del grupo C.
-->

<!-- 1 -->

**<u>Sevilla-Callejo, M.</u>; Mata Olmo, R. (2007).** “Introducción a las dinámicas territoriales en el área oriental del Parque Nacional y ANMI Cotapata (Depto. de La Paz, Bolivia)”. _Ecología en Bolivia_ 42(1): 34–47. <http://bit.ly/EB-42-1-p34>
<!-- impacto:
La revista tiene evaluación externa por pares, tiene un comité científico internacional, el contenido de la revista es exclusivo de investigación y se encuentra en repertorios y boletines bibliográficos vinculados a la geografia, en concreto esta dentro de LatIndex ademas de que se encuentra dentro de la Clasificación Integrada de Revistas Científicas (CIRC) dentro del grupo C.
sale como SciELO - WOS muestra 1 cita
 -->


<!-- LIBROS -->

## Libros

**Mata Olmo, R.; <u>Sevilla Callejo, M.</u>; Arteaga Cardeau, C.; Fernández Muñoz, S. (2010).** “Patrimonio Paisajístico“ en de Meer Lecha-Marzo, A. (Ed.) _Valoración del patrimonio territorial y paisajístico del valle del Nansa y Peñarrubia_, Santander, Fundación Marcelino Botín. Serie Patrimonio y Territorio. vol. VI, 384 pp. ISBN: 978-84-96655-61-4

**Sevilla Callejo, M. (2010).** _Organización territorial y campesinado en el Parque Nacional y Área Natural de Manejo Integrado Cotapata (Bolivia)_. Tesis doctoral, Madrid: Departamento de Geografía, Universidad Autónoma de Madrid, Dirección Rafael Mata Olmo. Defendida el 9 de septiembre de 2010, 412 pp. ( y anexos). ISBN: 978-84-694-3083-5. Disponible en <http://hdl.handle.net/10486/5726>

**Tello Ripa, B. (Coord.); Hungría Sánchez, P.; Sevilla Callejo, M. ; Fernandez García, F.; Fidalgo Hijano, C.; González Martín, J.A.; (2004).** _Prácticas de laboratorio de Geografía Física_. Madrid, Ediciones de la Universidad Autónoma de Madrid, 141 pp. ISBN 978-84-7477-913-8.

<!-- CAPÍTULOS DE LIBROS -->

## Capítulos de libros

**Suárez-Cebrián, A., Ochoa, H., Cámara-Menoyo, C., <u>Sevilla-Callejo, M.</u>;, Colaboradores de OpenStreetMap (2017).** “El uso de una plataforma cartográfica libre, OpenStreetMap, para el mapeado colaborativo de la ciclabilidad de Zaragoza. en Vergara Román, L. (Coord.) _La Bicicleta y la Ciudad_. Zaragoza, Colectivo Pedalea, pp. 89–102. ISBN: 978-84-697-7154-9
<!-- ESTABA disponible en: http://laciudaddelasbicis.org/cinco-ejes-cincuenta-comunicaciones-una-publicacion-la-ciudad-de-las-bicis-ya-en-libro/ -->

**<u>Sevilla-Callejo, M.;</u>; Zorrilla Alonso, O.; Colaboradores de OpenStreetMap (2015).** “Uso de OpenStreetMap (plataforma libre de datos geográficos) para mejorar la seguridad en la actividad senderista: el ejemplo de la red de senderos homologados en Las Merindades (Burgos)” en Allueva Torres, P.; Nasarre Sarmiento, J.M. (Coord.) _Retos del Montañismo en el siglo XXI. Congreso Internacional de Montañismo CIMA2015_. Zaragoza, Universidad de Zaragoza, pp. 242-255. ISBN: 978-84-608-4530-0 Disponible en http://issuu.com/bibliotecafedme/docs/librocima2015
<!-- 2 autocitas por Google Scholar -->

**Valero-Garcés, B.l.; González Sampériz, P.; Moreno, A.; GRUPO PALEOIPE: Aranbarri, J.; Barreiro, F.; Bartolomé, M.; Corella, J.P.; Frugone, M.,; García-Prieto, E.; Gil-Romera, G.; Jambrina, M.; Leunda, M.; Martí-Bono, C.; Morellón, M.,; Oliva-Urica, B.; Pérez-Sanz, A.; Pérez-Mejías, C.; Rico, M.T.,; <u>Sevilla-Callejo, M.</u> (2014).** "Paisajes y climas del último ciclo glacial en el NE de la Península ibérica: una visión desde la evolución de los glaciares, lagos y espeleotemas" en  J. Arnáez, J.; González-Sampériz, P.;  Lasanta, T.; Valero Garcés, B.L. (Eds). _Geoecología, cambio ambiental y paisaje. Homenaje al Profesor José María García Ruiz._ Logroño, Instituto Pirenaico de Ecología (CSIC) - Universidad de La Rioja, pp. 19-49. ISBN: 978-84-96487-83-3

**Valero-Garcés, B.L.; Oliva-Urcia, B.; Moreno Caballud, A.; Rico, M.T.; Mata, M.P.; Salazar-Rincón, A.; Rieradevall, M.; García-Ruiz, J.M.; Chueca Cía, J.; González-Sampériz, P.; Pérez-Sanz, A.; Salabarnada, A.; Pardo, A.; Sancho Marcén, C.; Barreiro-Lostres, F.; Bartolomé, M.; García-Prieto, E. ; Gil-Romera, G.; López Merino, L.; <u>Sevilla-Callejo, M.</u>; Tarrats, P. (2013).** "Dinámica glacial, clima y vegetación en el Parque Nacional de Ordesa y Monte Perdido durante el Holoceno" en L. Ramírez y B. Asencio (Eds). _Proyectos de investigación en parques nacionales: 2009-2012_ (2013) Organismo Autónomo Parques Nacionales (España), pp. 7- 37 ISBN: 978-84-8014-853-5. Disponible en: https://digital.csic.es/handle/10261/100566 <!-- referencia libro https://rebiun.baratz.es/rebiun/record/Rebiun03591785 -->

**Mata Olmo, R.; <u>Sevilla Callejo, M.</u> (2008).** "Ordenación del territorio y paisaje rural. El caso del Plan Territorial Insular de Menorca y los Contratos agrarios" en Molinero, F. (Ed.): _Espacios naturales protegidos. Actas del III Coloquio hispano-francés de Geografía Rural._ [CD-ROM] Baeza, Grupo de trabajo de Geografía Rural de la AGE, 17 pp. ISBN: 978-84-691-1464-3 <!-- NO TENGO COPIA -->

**<u>Sevilla Callejo, M.</u> (2004).** “Campesinado y nuevas dinámicas territoriales en el valle bajo del río Huarinilla (Parque Nacional y ANMI Cotapata, Bolivia)” en A. Maya Frades (Coord.): _¿Qué futuro para los espacios rurales? Actas del XII Coloquio de Geografía Rural_. León, Universidad de León, pp. 421-431. ISBN 84-9773-145-X

**<u>Sevilla Callejo, M.</u>; Mata Olmo, R.; Gil Romera, G. (2003).** “Infraestructuras y avance de la frontera agrícola en el Parque Nacional de Cotapata (Bolivia). Aplicación de las Tecnologías de la Información Geográfica” en J.L. Gurría Gascón et al. (Eds.): _IX Conferencia Iberoamericana de SIG_. Cáceres, Universidad de Extremadura. ISBN 84-688-3738-5.

**Mata Olmo, R.; Rodríguez Esteban, J. A.; <u>Sevilla Callejo, M.</u> (2002).** “Un SIG para el Plan de Ordenación de Menorca. Aspectos Ambientales y Paisajísticos” en J.L. García Cuesta (Coord.): _X Congreso de Métodos Cuantitativos, SIG y Teledetección._ Valladolid, Universidad de Valladolid. ISBN 84-87528-47-3.


<!-- Enlace a perfíl partucular de publicaciones científicas  -->

## Enlaces a mis perfiles de publicaciones

- [Scopus](http://www.scopus.com/authid/detail.uri?authorId=35975272000) (ID 35975272000) <!-- conecta con ORCiD -->
- [Google Scholar Citations](https://scholar.google.com/citations?user=_arx1GsAAAAJ)
- [Web of Science](https://app.webofknowledge.com/author/record/3594446) <!-- RECLAMAR como propio -->
- [Dialnet](https://dialnet.unirioja.es/servlet/autor?codigo=2621014) <!-- RECLAMAR como propio ¿? -->

<!-- 
ver más enlaces a páginas (OrCid) en curriculum_online_webs.md
-->
